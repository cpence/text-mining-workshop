# Voyant Tools Demo Instructions

This is roughly the demo that the group worked through for checking out the major features of Voyant Tools.

*   Start the application
    -   Unpack the ZIP file
    -   Double-click the VoyantServer.jar file.
        +   If you get a Mac OS X security/permissions warning, right-click the .jar file, click Open, and approve the security warning. You'll only have to do this once.
    -   The Voyant Server control screen should display, and your web browser should automatically open to the local Voyant page. [If you lose the link, click here to go back to it.](http://127.0.0.1:8888/)
*   Load the Austen corpus:
    -   Click 'Upload' in the bottom-left of the "Add Texts" box.
    -   Click 'Add...', and browse to the AustenCorpus.zip file.
    -   Click 'Reveal'.
*   This is the main Voyant screen, with some tools already displayed and a cluster of further tools available around the edges. First, let's get them all on the screen.
    -   Click the two-arrows icon in the top-right corner, which displays the third column of Voyant's interface. This right-hand column includes the Word Trends, Keywords in Context, and Words in Documents tools.
    -   Click the down-arrow on the far right of the bottom row in the left-hand column, to reveal the Words in the Entire Corpus tool.
    -   Click the down-arrow in the bottom row of the central column, to reveal the Corpus tool.
*   Let's look at some of these tools in more detail.
*   First, imagine that we're interested in how Austen uses some particular word. How about "life"?
    -   At the bottom of the "Words in the Entire Corpus" box, you'll see a small "Search" field. Click in that field, and type 'life', and press Enter.
    -   Now the list above will be filled with all the words in the corpus containing 'life'. (Note that lots of them have dashes in them -- this is something you'd want to notice and figure out how to deal with if you were basing some very serious analysis off of this demo. For now, we'll ignore it.)
    -   Click on 'life', the first result in the list.
*   Several things have now happened throughout the interface.
    -   Check out the central "Corpus Reader" panel. It's highlighted the first hit matching 'life' in the corpus, from early in _Emma._ The colored bars down the left-hand side that used to just indicate the positions of the various documents are now faded in and out, to correspond to the incidence of 'life' throughout the corpus.
    -   You can double-click one of those bars to jump to that point in the corpus. (The stronger the color, the more 'life' appears in that section.)
    -   On the right-hand side, the graph in Word Trends has been filled in, and we can now see that the word 'life' appears much more often in the juvenilia than in any of the published novels.
    -   Let's zoom in and see what some of these uses actually look like. Click the down-arrow next to Keywords in Context, and then click the point in the graph above for _Persuasion._ You'll get a full concordance listing of every occurrence of 'life', along with the immediate context before and after it.
*   Now, let's look at a comparative analysis.
    -   Head back to that search box at the bottom-left, the bottom row of the Words in the Entire Corpus tool.
    -   This time, type 'life, death' and hit enter. Check the check-boxes next to *both* life and death.
    -   Now you'll get a parallel graph up in Word Trends -- unsurprisingly, the two ideas track each other pretty closely throughout the corpus.
    -   For more details, you can pull up the Words in Documents tool at the bottom-right. (You may have to un-select and re-select 'life' and 'death' in the Words in the Entire Corpus tool to get the Words in Documents tool to re-calculate. This is a general quirk of Voyant -- sometimes you have to jiggle the cord to get it to update all of its views.)
    -   This will give you more detailed information about how the words distribute across the various documents. You can check, for example, the box next to 'life' beside 'Pride and Prejudice', and actually look at how the various uses of the term distribute *within* the document. Clicking one of those nodes in the graph will display that particular segment of P&P in the corpus reader window at center.
*   Now, let's look at the perennial word cloud.
    -   Aside: I hate word clouds. They're misused, overused, confusing, and don't allow inference.
    -   But anyway, people like them. What you have up here is a word cloud for the whole corpus.
    -   You'll note that there's a lot of uninteresting words here. To add a stop list, click the little gear in the top right of the Cirrus window. Under Stop Words List, select English (Taporware), and click OK.
    -   Now this looks like a more distinctively Austenite list. Mr., Mrs., and Miss are massive. Impressively you see the role of *thoughts* in Austen's corpus, with 'know', 'think', and 'thought' taking places not that far behind 'said'.
*   We can also do some more interesting analysis on the words themselves.
    -   Expand the Corpus tool at the bottom-center, and the Words in Documents tool at bottom-right.
    -   Double-click on _Emma_ to load it into the Words in Documents tool.
    -   Hover over one of the column headers in the Words in Documents tool, and click the down arrow. Deactivate 'Trend', and activate 'Z-Score' and 'Relative Difference'.
    -   Click the 'Relative Difference' column twice to sort by it, descending.
    -   Now you're looking at the words that are most common in _Emma_, _with respect to the rest of the corpus._ Of course, 'Emma' is one of them, as are lots of proper-names from the story. But it's perhaps interesting that 'Mr' is *almost* as distinctive of _Emma_ as is 'Emma'!
*   Now, here's where things get strange and the fact that this is a piece of software written by humanities people kicks in. There's *all sorts of tools* that you can't use unless you visit them directly by typing them into the URL bar. And then you have to re-upload your texts. This is annoying, and will be fixed in the upcoming version of Voyant that isn't quite ready yet.
    -   [Bubblelines](http://127.0.0.1:8888/tool/Bubblelines)
        +   Shows progression of a text through a corpus as bubbles; check out 'life' again for an example.
    -   [Collocation Grid](http://127.0.0.1:8888/tool/DocumentTypeCollocateFrequenciesGrid/)
        +   There's some interesting collocations for 'with' in this corpus.
    -   [Links](http://127.0.0.1:8888/tool/Links/)
        +   Same collocations data, but presented in a new format. Can be a little harder to use.
    -   [RezoViz](http://127.0.0.1:8888/tool/RezoViz/)
        +   Displays connections between organizations and places as extracted from your text.
        +   Can't handle the full Austen corpus (too big, perhaps?). Try uploading a smaller document.

