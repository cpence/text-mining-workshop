latex input:  mmd-beamer-header
Title:        Text Mining Workshop  
Author:       Charles H. Pence, LSU Philosophy  
Date:         March 8, 2016  
latex mode:   beamer  
LaTeX XSLT:   beamer
Theme:        Pittsburgh  
latex input:  mmd-beamer-begin-doc  
latex footer: mmd-beamer-footer  
Comment:      To compile these slides, you will need a functioning LaTeX
              system, an installation of MultiMarkdown, and to make the
              MMD TeX template files accessible to your LaTeX compiler.
              For instructions, see:
              https://fletcher.github.io/peg-multimarkdown/

### Follow Along

https://github.com/cpence/text-mining-workshop

**Head there now** and start downloading Voyant Server!

## Why Do Text Mining? Some Cool Examples

### Analyzing Style in Austen

Use of top thirty function words ("the", "of", "it", "I", etc.) sufficient to differentiate *all characters in all Austen novels.*

|                   |                       Word                     ||||||
Character           | we     | us     | very   | quite  | of     | in     |
 ------------------ | :----: | :----: | :----: | :----: | :----: | :----: |
Catherine (_NA_)    | 3.69   | 1.42   | 12.50  | 1.42   | 15.91  | 10.65  |
Henry (_NA_)        | 2.93   | 1.30   | 5.85   | 0.16   | 29.92  | 18.05  |
Anne (_P_)          | 5.30   | 1.85   | 6.92   | 0.69   | 25.60  | 17.76  |
Mrs. Smith (_P_)    | 1.51   | 0.50   | 4.77   | 0.25   | 25.60  | 15.06  |

### And in _Dracula_

![](hoover/stoker-dracula.png)

### Sentiment Analysis on Twitter: Vacation

![](twitter/vacation.png)

### Sentiment Analysis on Twitter: Suicide

![](twitter/suicide.png)

### Drug Interaction Discovery

![](drug-interaction/figure.jpg)

## The Big Tools: Web-Based Analysis Platforms

### Google Ngram Viewer: Demo!

https://books.google.com/ngrams

![](screenshots/ngrams.png)

### HathiTrust Bookworm: Demo!

http://bookworm.htrc.illinois.edu/

![](screenshots/bookworm.png)

### JSTOR Data for Research: Demo!

http://dfr.jstor.org/

![](screenshots/dfr.png)

## Assembling Your Own Datasets

### Books

*   Project Gutenberg (https://www.gutenberg.org/)
*   Google Books (https://books.google.com)
*   EEBO (http://quod.lib.umich.edu/e/eebogroup/)
*   ECCO (http://quod.lib.umich.edu/e/ecco/)
*   Evans (http://quod.lib.umich.edu/e/evans/)

### Journals

*   JSTOR DFR (http://dfr.jstor.org/)
*   Open Access:
    -   PMC Open Access Subset (https://www.ncbi.nlm.nih.gov/pmc/tools/openftlist/)
    -   PLoS (https://www.plos.org/publications/journals/)
    -   BMC (http://old.biomedcentral.com/about/datamining)

### Social Media

*   _Mining the Social Web,_ from O'Reilly
*   Twitter APIs (https://dev.twitter.com/)
*   Facebook APIs (https://developers.facebook.com/)

## Troubles with Access and Quality

### Copyrighted and For-Profit Content

*   For-profit journals
    -   Elsevier has a text-mining API
    -   Otherwise: negotiate contracts (ugh)
*   Contemporary books
    -   HathiTrust has them, but getting access can be difficult

### Google Ngram Worries

*   Digitization process is not complete
*   Worries about statistical significance of results, even in contemporary English
*   Worries about breadth of corpus, especially in early works
*   Worries about how we ought to reconstruct historical concepts

### Social Media and Representativeness

*   You don't know if your sample from Twitter is representative of all of Twitter
*   You don't know if your sample of public tweets is representative of the content of private tweets
*   You don't know if Twitter users are representative of the broader population

(boyd and Crawford, 2011, SSRN: 1926431)

### The 11th of the month?

![](11th-of-the-month/low-graph.png)

### But wait:

*   March llth
*   July IIth
*   May iith
*   August lith
*   January nth

### Fixed!

![](11th-of-the-month/fixed-graph.png)

### Congrefs

![](variant-spelling/congrefs.png)

### Blessed

*   blefsed
*   bleffcd
*   blefled
*   bleffed
*   bleflcd
*   blcfled
*   bleffcd

### Blessed

![](variant-spelling/blcfled.png)

### Automated named entity extraction

Extracting proper nouns from Austen's _History of England_:

*   "Lord Mounteagle" = LOCATION
*   "Essex", "Warwick" = ORGANIZATION (confused with universities?)
*   "Finis" = PERSON

Extracting parts of speech:

*   "AGAINST" = proper noun (confused by all-caps?)
*   "who sailed round the World", "round" = noun (confused by archaic usage)

## Running Text-Mining Tools Locally

### First, an aside

*   Voyant allows data export in portable formats (TXT, CSV, RTF, etc.)
    -   **Take your data with you!**
    -   Take notes on your corpus, your methods, your tool settings, etc.
*   Consider switching to scripting- or command-line-based tools as soon as you feel ready
    -   Why? Much easier for you to reproduce your own work later on!

### Voyant Tools: Demo!

![](screenshots/voyant.png)
