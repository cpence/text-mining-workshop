These examples are drawn from a slideshow presented by [David L. Hoover](https://wp.nyu.edu/davidlhoover/) at the [Digital Humanities Summer Institute](http://www.dhsi.org/) in 2012. Thanks, David!
