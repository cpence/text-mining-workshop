# Companion to Text Mining Workshop

*Charles H. Pence, Louisiana State University, Philosophy*  
*Last Update: March 8, 2016*

----------

### Are you at the workshop right now?

Then you need to **download Voyant Server** so you can follow along with our demo here in a little while! [Head to the Voyant Server site](http://docs.voyant-tools.org/resources/run-your-own/voyant-server/) and start downloading the ZIP file. Also, download [the Jane Austen corpus](AustenCorpus.zip?raw=true) we'll be feeding into Voyant.

----------

### Outline

*   [Why text mining?](#why-text-mining)
*   [The big buckets](#big-buckets-of-data)
*   [Putting your own data together](#assembling-your-own-data)
*   [Perils of data access and quality](#problems-of-access-and-quality)
*   [Local text-mining tools](#text-mining-tools-you-can-run-locally)
*   [Data and reproducibility](#data-and-reproducibility)

----------

### Why text mining?

*   [Burrows, _Computation into Criticism_ (1987)](http://www.amazon.com/Computation-into-Criticism-Austens-Novels/dp/0198128568)
    -   Top-thirty function words distinguishes the character dialogue of *all* characters in Austen's novels
    -   Compare, on the other hand, with Hoover's analysis of _Dracula_:  
        ![Tree](hoover/stoker-dracula.png?raw=true)
*   [Web-based Twitter sentiment analysis app](https://www.csc.ncsu.edu/faculty/healey/tweet_viz/tweet_app/)
    -   Sentiment graph for 'vacation':  
        ![Graph](twitter/vacation.png?raw=true)
    -   Sentiment graph for 'suicide':  
        ![Graph](twitter/suicide.png?raw=true)
*   [Text-mining paper abstracts to detect drug interactions](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3345566/)  
    ![Network of interactions](drug-interaction/figure.jpg?raw=true)

### Big buckets of data

*   [Google Ngram Viewer](https://books.google.com/ngrams)
    -   8M books (= 6% of all books ever published)
    -   [Basic information about the viewer, search methods, and its corpus](https://books.google.com/ngrams/info)
    -   [Raw access to the Ngram datasets (*warning:* massive, massive download ahead)](https://books.google.com/ngrams/datasets)
    -   [Most recent academic publication on the Ngram corpus, describing its part-of-speech tagging](http://aclweb.org/anthology/P/P12/P12-3029.pdf)
    -   [First major publication, describing the corpus in general](http://science.sciencemag.org/content/331/6014/176)
*   [HathiTrust Digital Library: Bookworm](http://bookworm.htrc.illinois.edu/)
    -   4.6M books searchable in this database (of HT's 13.9M total)
*   [JSTOR Data for Research](http://dfr.jstor.org/)
    -   9.3M journal articles

### Assembling your own data

*   [Project Gutenberg](https://www.gutenberg.org/)
    -   A repository of public-domain books, available in plain text. Most have been at least proofread by a team of volunteers, so the text is very often in good condition.
*   [Google Books](https://books.google.com)
    -   For public-domain books not appearing in Project Gutenberg, a scanned copy is very often available as a PDF from Google Books. Text can then be extracted from the PDF using a variety of tools. (Alternatively, you can re-process the optical character recognition (OCR) on the books using a tool such as [ABBYY FineReader.](http://www.abbyy.com/))
*   [EEBO-TCP](http://quod.lib.umich.edu/e/eebogroup/) / [ECCO-TCP](http://quod.lib.umich.edu/e/ecco/) / [Evans-TCP](http://quod.lib.umich.edu/e/evans/)
    -   EEBO: Early English Books Online, books written in English from 1475-1700, marked up in XML/TEI (just under 30,000 volumes)
    -   ECCO: Eighteenth-Century Collections Online, books written in English in the 18th century, marked up in XML/TEI (just over 2,000 volumes)
    -   Evans Early American Imprint Collection, books, pamphlets, and broadsides from America, 1640-1821, marked up in XML/TEI (just under 5,000 volumes)
*   [JSTOR DFR](http://dfr.jstor.org/)
    -   With an account, you may request from the administrators of the JSTOR DFR that you be able to download (limited amounts of) full-text articles via the DFR.
*   Open access journals
    -   [The PMC Open Access Subset](https://www.ncbi.nlm.nih.gov/pmc/tools/openftlist/)
    -   [PLoS Journals](https://www.plos.org/publications/journals/) are available in XML
    -   [BMC Journals](http://old.biomedcentral.com/about/datamining) may be downloaded in bulk
*   Social network data
    -   [_Mining the Social Web_, book from O'Reilly](https://github.com/ptwobrussell/Mining-the-Social-Web-2nd-Edition)

### Problems of access and quality

#### Getting your hands on the sources

*   For-profit journals
    -   [Elsevier has a text-mining API,](https://www.elsevier.com/about/company-information/policies/text-and-data-mining) but you have to contort your project to work with it
    -   Effectively all other publishers require negotiation of a contract
*   Contemporary books
    -   [HathiTrust](https://www.hathitrust.org/) has them, but access is limited and controlled (LSU is not a "friend" library)
*   Google Ngram worries
    -   Questions of statistical significance ([letter 1](http://dx.doi.org/10.1126/science.332.6025.35-b), [letter 2](http://dx.doi.org/10.1126/science.332.6025.35-c), [response](http://dx.doi.org/10.1126/science.332.6025.36-a))
    -   Questions of breadth of corpus ([blog post](http://www.prooffreader.com/2014/09/buck-naked-to-butt-naked-arms-to-anus.html) arguing that earlier works skew scientific/biblical)
    -   [Questions of reconstruction of historical concepts](http://tedunderwood.com/2012/08/25/how-not-to-do-things-with-words/)
*   Problems with the representativeness of social media data
    -   See [a provocative article by danah boyd and Kate Crawford](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1926431)

#### OCR error and other textual problems

*   [The 11th of the month](http://drhagen.com/blog/the-missing-11th-of-the-month/)
    -   [Original XKCD comic](https://xkcd.com/1140/)
    -   Graph of data as pulled from Google Ngrams:  
        ![Original data graph](11th-of-the-month/low-graph.png?raw=true)
    -   Graph of data after OCR error accounted for:  
        ![Fixed data graph](11th-of-the-month/fixed-graph.png?raw=true)
*   Variant typography and spelling
    -   Frequencies of 'congrefs' and 'Congrefs' in the corpus from 1700–1850:  
        ![Graph](variant-spelling/congrefs.png?raw=true)
    -   More complex: frequencies of 'blefsed', 'bleffcd', 'blefled', 'bleffed', 'bleflcd', 'blcfled', and 'bleffcd' in the corpus from 1650–1850:  
        ![Graph](variant-spelling/blcfled.png?raw=true)
    -   [An attempt at automated OCR correction rules for 17th- and 18th-century texts](http://usesofscale.com/gritty-details/basic-ocr-correction/)

#### Algorithmic extraction of metadata/tagging

*   Automated extraction can only be so good
    -   [Stanford NLP named entity extraction on Austen's _History of England_](nlp/Austen-HoE-named-entities.txt)
        +   "Lord Mounteagle" = LOCATION
        +   "Essex", "Warwick" = ORGANIZATION (confused with universities?)
        +   "Finis" = PERSON
    -   [Stanford NLP part-of-speech tagging on Austen's _History of England_](nlp/Austen-HoE-parts-of-speech.txt)
        +   "AGAINST" = proper noun (confused by all-caps?)
        +   "who sailed round the World", "round" = noun (confused by archaic usage)

### Text mining tools you can run locally

*   [Voyant Tools](http://voyant-tools.org/)
    -   [Local download version](http://docs.voyant-tools.org/resources/run-your-own/voyant-server/)
    -   [Introductory workshop](http://docs.voyant-tools.org/workshops/dh2015/)
    -   [List of all available tools](http://docs.voyant-tools.org/tools/)
    -   Links to some tools that can run on your local version of Voyant:
        +   [Bubblelines](http://127.0.0.1:8888/tool/Bubblelines)
        +   [Collocation Grid](http://127.0.0.1:8888/tool/DocumentTypeCollocateFrequenciesGrid/)
        +   [Links](http://127.0.0.1:8888/tool/Links/)
        +   [RezoViz](http://127.0.0.1:8888/tool/RezoViz/)
*   [The Intelligent Archive](http://www.newcastle.edu.au/research-and-innovation/centre/education-arts/cllc/intelligent-archive)
*   [David Hoover's Excel Text Analysis Pages](https://wp.nyu.edu/exceltextanalysis/)
    -   [The Zeta and Iota Spreadsheets](https://wp.nyu.edu/exceltextanalysis/zetaiotawidespectrum/) ([usage guide](https://wp.nyu.edu/exceltextanalysis/zetaiotawidespectrum/usingzetaiota/))
    -   [The Delta Spreadsheets](https://wp.nyu.edu/exceltextanalysis/deltaspreadsheets/) ([usage guide](https://wp.nyu.edu/exceltextanalysis/deltaspreadsheets/usingthedeltaspreadsheets/))  [not used in the workshop, but potentially also useful]

### Data and reproducibility

*   When using tools like Voyant, *make sure to export your data*
    -   CSV (comma-separated value) format can be read by Excel
    -   TXT/RTF (text or rich text) formats can be read by your text editor or Word
*   Also *document how you perform your analyses*
    -   Include what version of Voyant you use
    -   Lay out in detail what data sources you used, where and when you got your data, etc.
    -   Describe which tools you used and what settings you set
    -   Think about how you'd describe what you're doing to someone who hadn't ever used the tool before
*   Consider making the switch to command-line-based (or scripting-based) tools when you feel capable, as these provide much higher reproducibility
    -   See [Kieran Healy's case for reproducible tools in digital social sciences](http://kieranhealy.org/publications/plain-person-text/)

----------

### Want to add to this workshop?

If you have a resource that you think would be useful to participants in a workshop like this, feel free [to submit an issue on GitHub,](https://github.com/cpence/text-mining-workshop/issues/new) or fork the repository and send me a pull request!

----------

### License

All content here produced by Charles Pence (and not licensed in other ways as noted) is available under [the Creative Commons Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0).](https://creativecommons.org/licenses/by-sa/4.0/) For copyrights and credits for some of these images, check out the subdirectories in the repository. The Austen corpus is released under the terms of the [Project Gutenberg Terms of Use,](https://www.gutenberg.org/wiki/Gutenberg:Terms_of_Use) as the corpus is edited slightly (to remove licensing language, as it would skew results) from original Project Gutenberg source material.
